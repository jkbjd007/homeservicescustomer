import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:homeservicescustomer/ui/signin.dart';
import 'package:image_picker/image_picker.dart';
import "package:http/http.dart" as http;
import 'package:toast/toast.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  var _formkey = GlobalKey<FormState>();
  bool checkBoxValue = false;
  double _height;

  File _image;
  var proImage = null;
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      proImage = image;


    });

  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Colors.lightBlueAccent,
          )),
      body: Form(
        key: _formkey,
        child: ListView(
          children: <Widget>[

            Container(
              height: 190.0,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Stack(fit: StackFit.loose, children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: 160.0,
                              height: 160.0,
                              child:  _image != null
                                  ? ClipOval(
                                  child: Image.file(_image, height: 100,
                                      width: 100,
                                      fit: BoxFit.cover)
                              )
                                  : ClipOval(
                                  child: Image.asset('assets/logoo.png', height: 100,
                                      width: 100,
                                      fit: BoxFit.cover)
                              )

                          ),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 100.0, right: 100.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                  backgroundColor: Colors.lightBlueAccent,
                                  radius: 25.0,
                                  child: FlatButton(
                                    onPressed: (){
                                      getImage();

                                    },
                                    child: Center(
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                      ),
                                    ),
                                  )
                              )
                            ],
                          )),
                    ]),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
              child: TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'User Name',
                    errorStyle: TextStyle(
                      color: Colors.red,
                      fontSize: 15.0,
                    ),
                  ),
                  keyboardType: TextInputType.text,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "username is required";
                    }
                    return null;
                  }),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
              child: TextFormField(
                  controller: emailController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    errorStyle: TextStyle(
                      color: Colors.red,
                      fontSize: 15.0,
                    ),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "email is required";
                    }
                    return null;
                  }),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
              child: TextFormField(
                  controller: phoneController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Phone No',
                    errorStyle: TextStyle(
                      color: Colors.red,
                      fontSize: 15.0,
                    ),
                  ),
                  keyboardType: TextInputType.phone,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "phoneNo is required";
                    }
                    return null;
                  }),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
              child: TextFormField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    errorStyle: TextStyle(
                      color: Colors.red,
                      fontSize: 15.0,
                    ),
                  ),
                  keyboardType: TextInputType.text,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "password is required";
                    }
                    return null;
                  }),
            ),
            acceptTermsTextRow(),
            SizedBox(height: 20.0),
            SizedBox(height: 20.0),
            Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
              child: SizedBox(
                height: 50,
                child: RaisedButton(
                  onPressed: () {
                    setState(() {
                      if (_formkey.currentState.validate()) {
                        saveData(nameController.text, emailController.text, phoneController.text, passwordController.text, proImage);
                      }
                    });
                  },
                  child: Text(
                    'Sign up',
                    style: TextStyle(fontSize: 22.0),
                  ),
                  textColor: Colors.white,
                  color: Colors.lightBlueAccent,
                ),
              ),
            ),
            SizedBox(height: 10.0),
            signInTextRow(),
          ],
        ),
      ),
    );
  }

  Widget acceptTermsTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 100.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Checkbox(
              activeColor: Colors.lightBlueAccent[200],
              value: checkBoxValue,
              onChanged: (bool newValue) {
                setState(() {
                  checkBoxValue = newValue;
                });
              }),
          Text(
            "I accept all terms and conditions",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              letterSpacing: 1.5,
              color: Colors.lightBlueAccent,
              fontSize: 13.5,
            ),
          ),
        ],
      ),
    );
  }

  Widget signInTextRow() {
    return Container(
      margin: EdgeInsets.only(top: _height / 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Already have an account?",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              letterSpacing: 1.5,
              color: Colors.lightBlueAccent,
              fontSize: 13.5,
            ),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignInScreen(),
                  ));
              print("Routing to Sign in screen");
            },
            child: Text(
              "Sign in",
              style: TextStyle(
//                fontFamily: Constants.OPEN_SANS,
                fontWeight: FontWeight.w800,
                letterSpacing: 1.5,
                color: Colors.deepPurple,
                fontSize: 15.5,
              ),
            ),
          )
        ],
      ),
    );
  }

  saveData(String name, String email, String phone, String password, File profileImage) async{

    var request = http.MultipartRequest("POST", Uri.parse("http://192.168.8.104:3000/saveUsers"));
    var pic = await http.MultipartFile.fromPath("profileImage", profileImage.path);
    request.files.add(pic);
    request.fields["name"] = name;
    request.fields["email"] = email;
    request.fields["phone"] = phone;
    request.fields["password"] = password;
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print("Image"+responseString);
    Map myData = json.decode(responseString);
    if(myData['message'] == "Account Created..."){
      Toast.show(myData["message"], context, duration: Toast.LENGTH_LONG, gravity:  Toast.CENTER);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => SignInScreen(),
          ));
    }
    else{
      Toast.show(myData["error"], context, duration: Toast.LENGTH_LONG, gravity:  Toast.CENTER);
    }
  }

}
