import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import "package:http/http.dart" as http;

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool _status = true;
  Map data = {};
  final FocusNode myFocusNode = FocusNode();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  bool _obscureText = true;
  File _image;
  var proImage = null;
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      proImage = image;


    });
//    updateProfilePicture(proImage, data["id"]);

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data : ModalRoute.of(context).settings.arguments;
    _nameController.text = data['name'];
    _emailController.text = data['email'];
    _phoneController.text = data['phone'];
    _passwordController.text = data['password'];
   setState(() {
     //proImage = data['profileImage'];
   });
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Profile Setting"),
      ),
        body: Container(
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    height: 190.0,
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[

                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Stack(fit: StackFit.loose, children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    width: 160.0,
                                    height: 160.0,
                                    child:  _image != null
                                        ? ClipOval(
                                        child: Image.file(_image, height: 100,
                                            width: 100,
                                            fit: BoxFit.cover)
                                    )
                                        : ClipOval(
                                      child: Image.network('http://192.168.10.17/RestApi/uploads/'+data['profileImage'], height: 100,
                                          width: 100,
                                          fit: BoxFit.cover)
                                    )

                                ),
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 100.0, right: 100.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    CircleAvatar(
                                      backgroundColor: Colors.green[800],
                                      radius: 25.0,
                                      child: FlatButton(
                                        onPressed: (){
                                          getImage();

                                        },
                                        child: Center(
                                          child: Icon(
                                            Icons.camera_alt,
                                            color: Colors.white,
                                          ),
                                        ),
                                      )
                                    )
                                  ],
                                )),
                          ]),
                        )
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        'Parsonal Information',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      _status ? _getEditIcon() : Container(),
                                    ],
                                  )
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        'Name',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Flexible(
                                    child: TextField(
                                      decoration: const InputDecoration(
                                        hintText: "Enter Your Name",
                                      ),
                                      enabled: !_status,
                                      autofocus: !_status,
                                      controller: _nameController,

                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        'Email ID',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: TextField(
                                      decoration: const InputDecoration(
                                          hintText: "Enter Email ID"),
                                      enabled: !_status,
                                      controller: _emailController,
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        'Mobile',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Flexible(
                                    child: TextField(
                                      decoration: const InputDecoration(
                                          hintText: "Enter Mobile Number"),
                                      enabled: !_status,
                                      controller: _phoneController,
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      child: Text(
                                        'Password',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    flex: 2,
                                  ),

                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 10.0),
                                      child: TextField(
                                        obscureText: _obscureText,
                                        controller: _passwordController,
                                        decoration: InputDecoration(
                                          labelText: "Password",
                                          enabled: !_status,
                                          suffixIcon: FlatButton(
                                              onPressed: _toggle,
                                              child: new Icon(_obscureText ? Icons.visibility : Icons.visibility_off, color: Colors.green,)),
                                        ),
                                      ),
                                    ),
                                    flex: 2,
                                  ),

                                ],
                              )),
                          !_status ? _getActionButtons() : Container(),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: RaisedButton(
                    child: Text("Save"),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: () {

                      setState(() {
                        _status = true;
                        FocusScope.of(context).requestFocus(FocusNode());
                        //Toast.show('profile updated', context, duration: Toast.LENGTH_LONG, gravity:  Toast.CENTER);
                      });
//                      updateRecord(data['id'], _nameController.text, _emailController.text, _phoneController.text, _passwordController.text, proImage);
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: RaisedButton(
                    child: Text("Cancel"),
                    textColor: Colors.white,
                    color: Colors.red,
                    onPressed: () {
                      setState(() {
                        _status = true;
                        FocusScope.of(context).requestFocus(FocusNode());
                      });
                      //updateProfilePicture(proImage, data["id"]);
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.green[800],
        radius: 14.0,
        child: Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }

//  updateRecord(String id, String name, String email, String phone, String password, File profileImage) async{
//    Map data1 = {
//      "name": name,
//      "email": email,
//      "phone": phone,
//      "password": password,
//      "profileImage":profileImage,
//      "id": id
//    };
//
//
//    _nameController.text = name;
//    _emailController.text = email;
//    _phoneController.text = phone;
//    _passwordController.text = password;
//
//    var request = http.MultipartRequest("PUT", Uri.parse("http://192.168.10.17:3000/updateUserData"));
//
//    request.fields["name"] = name;
//    request.fields["email"] = email;
//    request.fields["phone"] = phone;
//    request.fields["password"] = password;
//    request.fields["id"] = id;
//
////    var pic = await http.MultipartFile.fromPath("profileImage", profileImage?? true );
////    request.files.add(pic);
//    var response = await request.send();
//    var responseData = await response.stream.toBytes();
//    var responseString = String.fromCharCodes(responseData);
//    print(responseString);
//    Map isSave = json.decode(responseString);
//
//    Toast.show(isSave["message"], context, duration: Toast.LENGTH_LONG, gravity:  Toast.CENTER);
//
//
//    LoginInfo info = new LoginInfo();
//    info.setId = id.toString();
//    info.setName = name;
//    info.setEmail = email;
//    info.setPhone = phone;
//    info.setPassword = password;
//    info.setProfileImage = data["profileImage"];
//    //info.setProfileImage = profileImage.path.split('/').last;
//    Navigator.pushReplacementNamed(context, '/homePage', arguments: {
//      'id': info.id,
//      'name': info.name,
//      'email': info.email,
//      'phone': info.phone,
//      'password': info.password,
//      'profileImage': info.profileImage
//    });
//
//
////    var response = await http.put("http://192.168.10.8:3000/students", body: data);
////    debugPrint(response.toString());
////    if(response.statusCode == 200){
////      Map data1 = json.decode(response.body);
////      if(data1['message'] == "updated"){
////        Toast.show(data1['message'], context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
////      }
////      else{
////        Toast.show(response.toString(), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
////      }
////    }
////    else{
////      debugPrint(response.body);
////      Toast.show("status code 404", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
////    }
//  }
//
//  updateProfilePicture(File profileImage, String id) async{
//
//    var request = http.MultipartRequest("PUT", Uri.parse("http://192.168.10.17:3000/updateProfilePicture"));
//    var pic = await http.MultipartFile.fromPath("profileImage", profileImage.path);
//    request.files.add(pic);
//    request.fields["id"] = id;
//    var response = await request.send();
//    var responseData = await response.stream.toBytes();
//    var responseString = String.fromCharCodes(responseData);
//    print("Image"+responseString);
//    Map myData = json.decode(responseString);
//    Toast.show(myData["message"], context, duration: Toast.LENGTH_LONG, gravity:  Toast.CENTER);
//
//    LoginInfo info = new LoginInfo();
//    info.setId = id.toString();
//    info.setName = _nameController.text;
//    info.setEmail = _emailController.text;
//    info.setPhone = _phoneController.text;
//    info.setPassword = _passwordController.text;
//    info.setProfileImage = profileImage.path.split('/').last;
//
//    Navigator.pushReplacementNamed(context, '/homePage', arguments: {
//      'id': info.id,
//      'name': info.name,
//      'email': info.email,
//      'phone': info.phone,
//      'password': info.password,
//      'profileImage': info.profileImage
//    });
//
//
////    var request = http.MultipartRequest('PUT', Uri.parse("http://192.168.10.16:3000/updateProfilePicture"));
////    request.files.add(
////        http.MultipartFile(
////            'profileImage',
////            File(profileImage.path).readAsBytes().asStream(),
////            File(profileImage.path).lengthSync(),
////            filename: profileImage.path.split("/").last
////        )
////    );
////    request.fields["id"] = id;
////    var res = await request.send();
////    //Map isSaved = json.decode(res.toString());
////    Toast.show(res.toString(), context, duration: Toast.LENGTH_LONG, gravity:  Toast.CENTER);
////
//
//
//  }
}
