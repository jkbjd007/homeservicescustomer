import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:homeservicescustomer/drawer/drawer_home.dart';
import 'package:homeservicescustomer/model/login_data.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:stopper/stopper.dart';
import "package:http/http.dart" as http;

class HomePage extends StatefulWidget {
  String id;
  String name;
  String email;
  String phone;
  String password;
  HomePage({this.id,this.name,this.phone,this.email,this.password});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeMap(id: widget.id,name: widget.name,email: widget.email,phone: widget.phone,password: widget.password,),
    );
  }
}

class HomeMap extends StatefulWidget {
  String id;
  String name;
  String email;
  String phone;
  String password;
  HomeMap({this.id,this.name,this.phone,this.email,this.password});
  @override
  _HomeMapState createState() => _HomeMapState();
}

class _HomeMapState extends State<HomeMap> {
//  final List<IconData> icons = [
//    Icons.sentiment_very_dissatisfied,
//    Icons.home,
//    Icons.drafts,
//    Icons.backspace,
//  ];
//
  final List<String> service = [];

  Map jsonData;
  List userData;

  GoogleMapController _googleMapController;
  static LatLng _initialPosition = LatLng(38.008, 41.57849);
  Location _location = Location();
  StreamSubscription _locationSubscription;
  Marker marker;
  Circle circle;
  Map data;
  Timer timer;
  String latitude;
  String longitude;
  LoginInfo _info = LoginInfo();


  // intialize the controller on Map Create
  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _googleMapController = controller;
      _location.onLocationChanged().listen((l) {
        _googleMapController.animateCamera(
          CameraUpdate.newCameraPosition(CameraPosition(
              target: LatLng(l.latitude, l.longitude), zoom: 15)),
        );
      });
    });
  }

  Future<Uint8List> getMarker() async {
    ByteData byteData =
        await DefaultAssetBundle.of(context).load("assets/images/logoo.png");
    return byteData.buffer.asUint8List();
  }

  Future showServices() async {
    http.Response response =
        await http.get("http://192.168.43.238:3000/getServices");
    jsonData = json.decode(response.body);
    setState(() {
      userData = jsonData['data'];
    });
    debugPrint(userData.toString());
  }

  void getCurrentLocation() async {
    try {
      Uint8List imageData = await getMarker();
      var location = await _location.getLocation();

      updateMarkerAndCircle(location, imageData);

      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }

      _locationSubscription =
          _location.onLocationChanged().listen((newLocalData) {
        if (_googleMapController != null) {
          _googleMapController.animateCamera(CameraUpdate.newCameraPosition(
              new CameraPosition(
                  bearing: 192.8334901395799,
                  target: LatLng(newLocalData.latitude, newLocalData.longitude),
                  tilt: 0,
                  zoom: 14.00)));
          updateMarkerAndCircle(newLocalData, imageData);
        }
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        showToast("Permission Denied");
      }
    }
  }

  void updateMarkerAndCircle(LocationData newLocalData, Uint8List imageData) {
    LatLng latlng = LatLng(newLocalData.latitude, newLocalData.longitude);
    latitude = newLocalData.latitude.toString();
    longitude = newLocalData.longitude.toString();
    debugPrint("&&&&&&&&&&&&***************&&&&&&&&&" +
        latitude +
        "*********" +
        longitude);

    this.setState(() {
      marker = Marker(
        markerId: MarkerId("home"),
        position: latlng,
        infoWindow: InfoWindow(title: '${latlng}'),
      );
      circle = Circle(
          circleId: CircleId("car"),
          radius: newLocalData.accuracy,
          zIndex: 1,
          strokeColor: Colors.lightBlueAccent,
          center: latlng,
          fillColor: Colors.blue.withAlpha(70));
    });
  }

  @override
  void initState() {
    print("data from parent");
    print(" user_id = ${widget.id}");
    print(" user_nane = ${widget.name}");
    print(" user_email = ${widget.email}");

    getCurrentLocation();
    showServices();
    super.initState();
//    timer = Timer.periodic(Duration(seconds: 5),
//        (Timer t) => updateLatlng(latitude, longitude, _info.id));
  }

  @override
  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
//    timer?.cancel();
    super.dispose();
  }

//
  Widget serviceSelectionBar(BuildContext context, double h) {
    return Column(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: 45.0,
          child: MaterialButton(
            color: Colors.lightBlueAccent,
            child: Text(
              "Select Service",
              style: TextStyle(color: Colors.white, fontSize: 20.0),
            ),
            onPressed: () {
              showServices();
              showStopper(
                context: context,
                stops: [0.4 * h, h],
                builder: (context, scrollController, scrollPhysics, stop) {
                  return ClipRRect(
                    borderRadius: stop == 0
                        ? BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          )
                        : BorderRadius.only(),
                    clipBehavior: Clip.antiAlias,
                    child: Container(
                      color: Colors.white,
                      child: CustomScrollView(
                        slivers: <Widget>[
                          SliverAppBar(
                            title: Center(
                                child: Text("Which Service do You Need?")),
                            backgroundColor: Colors.lightBlueAccent[600],
                            automaticallyImplyLeading: false,
                            primary: false,
                            floating: true,
                            pinned: true,
                          ),
                          SliverList(
                            delegate: SliverChildBuilderDelegate(
                              (context, services_id) => Column(
                                children: <Widget>[
                                  ListTile(
                                    leading: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          30, 0, 0, 0),
                                      child: Image.network(
                                          "http://192.168.43.238/Adminpanel/uploads/" +
                                              userData[services_id]['icon']),
                                    ),
                                    title: Text(
                                      '${userData[services_id]['title']}',
                                      style: TextStyle(
                                        color: Colors.lightBlueAccent,
                                        fontSize: 20.0,
                                      ),
                                    ),
                                    subtitle: Text(
                                      '${userData[services_id]['per_hour_price']}',
                                      style: TextStyle(
                                        color: Colors.deepPurpleAccent,
                                        fontSize: 17.0,
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                      showToast(
                                          'you click index: ${service[services_id]}');
                                    },
                                  ),
                                ],
                              ),
                              childCount: userData.length,
                            ),
                          )
                        ],
                        controller: scrollController,
                        physics: scrollPhysics,
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
        SizedBox(
          height: 7,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 45.0,
          child: MaterialButton(
            onPressed: () {},
            color: Colors.lightBlueAccent,
            child: Text(
              "Confirm",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        )
      ],
    );
  }

  void showToast(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blue[600],
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  Widget build(BuildContext context) {
    data =  ModalRoute.of(context).settings.arguments;
//    data.isNotEmpty ? data :
    debugPrint(data.toString());
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: Builder(
        builder: (context) {
          final h = MediaQuery.of(context).size.height;
          return Stack(
            children: <Widget>[
              GoogleMap(
                markers: Set.of((marker != null) ? [marker] : []),
                circles: Set.of((circle != null) ? [circle] : []),
                initialCameraPosition: CameraPosition(
                  target: _initialPosition,
                  zoom: 15,
                ),
                onMapCreated: _onMapCreated,
                mapType: MapType.normal,
                myLocationEnabled: true,
                compassEnabled: false,
              ),
              Padding(
                padding: EdgeInsets.all(16.0),
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        serviceSelectionBar(context, h),
                      ],
                    )),
              ),
            ],
          );
        },
      ),
      drawer: DrawerHome(),
    );
  }

//  updateLatlng(String id, String latitude, String longitude) async {
//    Map data = {"latitude": latitude, "longitude": longitude, "id": id};
//    var response =
//        await http.put("http://192.168.43.238:3000/updateLatlng", body: data);
//    debugPrint(response.toString());
//    debugPrint("##############################" +
//        "***" +
//        latitude +
//        "***" +
//        longitude +
//        "***" +
//        _info.id);
//    debugPrint("#################" + latitude + "*********" + longitude);
//    if (response.statusCode == 200) {
//      Map data1 = json.decode(response.body);
//      if (data1['message'] == "success") {
////        Toast.show(data1['message'], context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
//        showToast(data1['message']);
//      } else {
////        Toast.show(response.toString(), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
//        showToast(response.toString());
//      }
//    } else {
//      debugPrint(response.body);
////      Toast.show("status code 404", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
//      showToast('status code 404');
//    }
//  }
}
