final String SIGN_IN = 'signin';
final String SIGN_UP ='signup';
final String SPLASH_SCREEN ='splashscreen';
final String Mainpage = 'mainPage';
final String Landing_Page ='landingPage';

class Constants {
  static const String POPPINS = "Poppins";
  static const String OPEN_SANS = "OpenSans";
}