class WorkerData {
  int w_id;
  String name;
  String email;
  String phone;
  String cnic;
  String password;
  String address;
  String job_type;
  String gender;
  String profile_image;
  String lati;
  String longi;

  WorkerData(this.w_id, this.name, this.email, this.phone, this.cnic,
      this.password, this.address, this.job_type, this.gender,
      this.profile_image, this.lati, this.longi);


}